module.exports = {
  parser: '@typescript-eslint/parser', // Specifies the ESLint parser
  plugins: ['@typescript-eslint', 'react-native', 'import'],
  parserOptions: {
    sourceType: 'module',
  },
  extends: [
    'airbnb',
    'plugin:@typescript-eslint/recommended',
    'plugin:react/recommended',
    'plugin:import/typescript',
  ],
  overrides: [
    {
      files: ['*.ts', '*.tsx'],
      extends: [
        'plugin:@typescript-eslint/eslint-recommended',
        'plugin:@typescript-eslint/recommended',
      ],
      parser: '@typescript-eslint/parser',
      plugins: ['@typescript-eslint'],
      rules: {
        '@typescript-eslint/no-non-null-assertion': 'off',
      },
    },
    {
      files: ['*/logic/modules/**/slice.ts'],
      rules: { 'no-param-reassign': ['error', { props: true, ignorePropertyModificationsFor: ['state'] }] },
    },
  ],
  settings: {
    'import/parsers': {
      '@typescript-eslint/parser': ['.ts', '.tsx', '.js', '.jsx'],
    },
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
      },
      typescript: {
        alwaysTryTypes: true, // always try to resolve types under `<root>@types` directory even it doesn't contain any source code, like `@types/unist`
        // Choose from one of the "project" configs below or omit to use <root>/tsconfig.json by default
        // use <root>/path/to/folder/tsconfig.json
        project: './tsconfig.json',
      },
    },
  },
  rules: {
    'no-use-before-define': 'off',
    '@typescript-eslint/no-use-before-define': ['error'],
    'react/forbid-prop-types': [2, { forbid: [], checkContextTypes: true, checkChildContextTypes: true }],
    'react/jsx-filename-extension': [1, { extensions: ['.tsx'] }],
    'import/prefer-default-export': 0,
    'react/destructuring-assignment': [0],
    'jsx-a11y/click-events-have-key-events': 0,
    'global-require': 0,
    'import/extensions': 0,
    'max-len': ['error', { code: 170, tabWidth: 2 }],
    'import/no-named-as-default': 0,
    'import/no-named-default': 0,
    'no-underscore-dangle': 0,
    'react/jsx-props-no-spreading': 0,
    'linebreak-style': ['error', 'unix'],
    'no-shadow': 'off',
    '@typescript-eslint/no-shadow': ['error'],
  },
};
