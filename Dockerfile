FROM node:16

WORKDIR /usr/src/app

COPY package*.json ./

RUN yarn install

COPY . .

EXPOSE 19000 19001 19002 19006

ENTRYPOINT ["npm"]
CMD ["run","web:linux"]
