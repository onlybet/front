# OnlyBet - front

Application expo, axios, native-base.

**Commands :**

```bash
  # run global site
  yarn run start

  # run web only
  yarn run web

  # run web for linux
  yarn run web:linux
```

## Architecture

l'application est divisée en 3 parties majeures :

### Logic

**logic/** tous les éléments de logique de l'application (api, redux, utilitaires)

- **api/** le dossier de configuration d'axios permettant de faire les appels api
- **modules/x** contient un dossier pour chaque modules de l'application (leagues, matchs, utilisateurs). Chaque dossier contient un fichier pour les appels api, et des fichiers pour créer sa slice redux.
- **store/** contient les fichiers de configuration pour le store (redux) et l'accès au localStorage et autres types de stockage de données.

### Models

**models/** ce dossier contient toutes les interfaces et modèles de l'application
- **rest/x** ce dossier contient toutes les interface et modèles correspondant aux schémas du back

### Rendering
**rendering/** tous les éléments visuels de l'application
- **components/** dossier parent du design atomic (atoms / molecules / organisms / etc)
- **config/** configuration des vues (router, navigation, AppContainer)
- **contexts/** tous les contexts utilisables dans react
- **theme/** la configuration du theme de l'application (typography, colors, components native-base, etc)
- **views/** les pages !
