module.exports = (api) => {
  api.cache(true);
  return {
    presets: ['babel-preset-expo'],
    plugins: [
      [
        'module-resolver',
        {
          alias: {
            '@components': './rendering/components/index.ts',
            '@theme': './rendering/theme/index.ts',
            '@config': './rendering/config/index.ts',
            '@navigation': './rendering/config/Navigation.tsx',
            '@views': './rendering/views/index.ts',
            '@models/rest': './models/rest/index.ts',
            '@api': './logic/api/index.ts',
            '@models': './models/index.ts',
            '@logic': './logic',
            '@contexts': './rendering/contexts/index.ts',
          },
        },
      ],
      ['inline-dotenv'],
    ],
  };
};
