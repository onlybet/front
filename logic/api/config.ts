import axios, { AxiosInstance } from 'axios';

interface AxiosResponseError {
  data?: Record<string, unknown>;
  headers: Record<string, unknown>;
  status: number;
  statusText: string;
  request: {
    responseURL: string;
    response: string;
    responseText: string;
    responseType: string;
    responseXML: string;
    status: number;
    statusText: string;
    timeout: number;
    withCredentials: boolean;
  };
  config: {
    baseURL: string;
    data: string;
    headers: Record<string, unknown>;
    maxBodyLength: number;
    maxContentLength: number;
    method: string;
    timeout: number;
    url: string;
    xsrfCookieName: string;
    xsrfHeaderName: string;
  };
}

export interface AxiosError {
  message: string;
  code: number;
  data?: Record<string, unknown>;
  response?: AxiosResponseError;
}

const serializedResponse = (e: AxiosResponseError): AxiosResponseError => ({
  data: e.data,
  headers: e.headers,
  status: e.status,
  statusText: e.statusText,
  request: {
    responseURL: e.request.responseURL,
    response: e.request.response,
    responseText: e.request.responseText,
    responseType: e.request.responseType,
    responseXML: e.request.responseXML,
    status: e.request.status,
    statusText: e.request.statusText,
    timeout: e.request.timeout,
    withCredentials: e.request.withCredentials,
  },
  config: {
    baseURL: e.config.baseURL,
    data: e.config.data,
    headers: e.config.headers,
    maxBodyLength: e.config.maxBodyLength,
    maxContentLength: e.config.maxContentLength,
    method: e.config.method,
    timeout: e.config.timeout,
    url: e.config.url,
    xsrfCookieName: e.config.xsrfCookieName,
    xsrfHeaderName: e.config.xsrfHeaderName,
  },
});

export const initAxios = (): AxiosInstance => {
  // set global requests URL
  axios.defaults.baseURL = process.env.API_URI;

  // convert axios requests errors to readables & serialized ones
  axios.interceptors.request.use((config) => config, (e): Promise<AxiosError> => {
    let error: Record<string, unknown> = { message: e.message, code: 408 };
    if (e.response) {
      error = {
        ...error, response: serializedResponse(e.response), data: e.response.data, code: e.response.status,
      };
    }
    return Promise.reject(error);
  });

  // convert axios responses errors to readables & serialized ones
  axios.interceptors.response.use((response) => response, (e): Promise<AxiosError> => {
    let error: Record<string, unknown> = { message: e.message, code: 408 };

    if (e.response) {
      error = {
        ...error, response: serializedResponse(e.response), data: e.response.data, code: e.response.status,
      };
    }

    return Promise.reject(error);
  });

  return axios.create();
};
