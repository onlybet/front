import leagues from '@logic/modules/leagues/api';
import matches from '@logic/modules/matches/api';
import user from '@logic/modules/user/api';

export const useApi = () => ({ api: { leagues, matches, user } });
