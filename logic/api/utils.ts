import axios from 'axios';

// add authorization token to every headers of axios requests
export const setGlobalApiAuthorizationToken = (authorizationToken: string): void => {
  axios.defaults.headers.common.Authorization = authorizationToken;
};
