import axios, { AxiosResponse } from 'axios';
import { League } from '@models';
import {
  toLeague, RestLeague, RestSelfUser,
} from '@models/rest';

const getAll = (): Promise<Array<League>> => axios.get('/competitions/foot/all').then(
  (response: AxiosResponse<Array<RestLeague>>) => response.data.map((data) => toLeague(data)),
);

//  const searchLeagues = (value: string): Promise<Array<League>> => axios.get(`/competitions/foot/search/${value}`).then(
const searchLeagues = (value: string): Promise<Array<League>> => axios.get(`/competitions/foot/name/${value}`).then(
  (response: AxiosResponse<Array<RestLeague>>) => response.data.map((data) => toLeague(data)),
);

const getLeague = (value: string): Promise<League> => axios.get(`/competitions/foot/${value}`).then(
  (response: AxiosResponse<RestLeague>) => toLeague(response.data),
);

const getFavorites = (): Promise<Array<League>> => axios.get('/user/favorites/competitions').then(
  async (response: AxiosResponse<Array<number>>) => {
    const leagues = [];
    // eslint-disable-next-line no-restricted-syntax
    for await (const league of response.data) {
      const result = await getLeague(league.toString());
      leagues.push(result);
    }
    return leagues;
  },
);

const addFavorite = (value: string): Promise<Array<League>> => axios.patch(`/user/favorites/competitions/${value}`).then(
  async (response: AxiosResponse<RestSelfUser>) => {
    const user = response.data;
    const leagues = [];
    // eslint-disable-next-line no-restricted-syntax
    for await (const league of user.favorites.competitions) {
      const result = await getLeague(league.toString());
      leagues.push(result);
    }
    return leagues;
  },
);

const deleteFavorite = (value: string): Promise<Array<League>> => axios.delete(`/user/favorites/competitions/${value}`).then(
  async (response: AxiosResponse<RestSelfUser>) => {
    const user = response.data;
    const leagues = [];
    // eslint-disable-next-line no-restricted-syntax
    for await (const league of user.favorites.competitions) {
      const result = await getLeague(league.toString());
      leagues.push(result);
    }
    return leagues;
  },
);

export default {
  getAll, searchLeagues, getLeague, getFavorites, addFavorite, deleteFavorite,
};
