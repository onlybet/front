import { createSlice } from '@reduxjs/toolkit';
import { initialState, ILeaguesState } from './state';
import {
  getAllLeagues, getFavoriteLeagues, addFavorite, removeFavorite, searchLeagues, getLeague, init,
} from './thunks';

const leaguesSlice = createSlice({
  name: 'leagues',
  initialState,
  reducers: {
    setLeagues: (state, { payload }): ILeaguesState => ({ ...state, data: payload }),
  },
  extraReducers: (builder) => {
    // when getLeague request starts, execute reducer
    builder.addCase(getLeague.pending, (state) => ({ ...state, selected: { data: null, loading: true } }));
    builder.addCase(getLeague.rejected, (state) => ({ ...state, selected: { ...state.selected, loading: false } }));
    builder.addCase(getLeague.fulfilled, (state, { payload }) => ({ ...state, selected: { ...state.selected, data: payload, loading: false } }));
    // when getAllLeagues request starts, execute reducer
    builder.addCase(getAllLeagues.pending, (state) => ({ ...state, loading: true }));
    builder.addCase(getAllLeagues.rejected, (state) => ({ ...state, loading: false }));
    builder.addCase(getAllLeagues.fulfilled, (state, { payload }) => ({ ...state, data: payload, loading: false }));
    // when getFavoriteLeagues request starts, execute reducer
    builder.addCase(getFavoriteLeagues.pending, (state) => ({ ...state, favorites: { ...state.favorites, loading: true } }));
    builder.addCase(getFavoriteLeagues.rejected, (state) => ({ ...state, loading: false }));
    builder.addCase(getFavoriteLeagues.fulfilled, (state, { payload }) => ({
      ...state,
      favorites: {
        ...state.favorites,
        data: payload,
        loading: false,
      },
    }));
    // when init request starts, execute reducer
    builder.addCase(init.pending, (state) => ({ ...state, favorites: { ...state.favorites, loading: true } }));
    builder.addCase(init.rejected, (state) => ({ ...state, loading: false }));
    builder.addCase(init.fulfilled, (state, { payload }) => ({
      ...state,
      favorites: {
        ...state.favorites,
        data: payload,
        loading: false,
      },
    }));
    builder.addCase(addFavorite.fulfilled, (state, { payload }) => ({
      ...state,
      favorites: {
        ...state.favorites,
        data: payload,
      },
    }));
    builder.addCase(removeFavorite.fulfilled, (state, { payload }) => ({
      ...state,
      favorites: {
        ...state.favorites,
        data: payload,
      },
    }));
    builder.addCase(searchLeagues.pending, (state) => ({
      ...state,
      searched: {
        data: [],
        loading: true,
      },
    }));
    builder.addCase(searchLeagues.rejected, (state) => ({
      ...state,
      searched: {
        data: [],
        loading: false,
      },
    }));
    builder.addCase(searchLeagues.fulfilled, (state, { payload }) => ({
      ...state,
      searched: {
        data: payload,
        loading: false,
      },
    }));
  },
});

export const leaguesActions = {
  ...leaguesSlice.actions, getAllLeagues, getFavoriteLeagues, addFavorite, removeFavorite, searchLeagues, getLeague, init,
};
export const leaguesReducer = leaguesSlice.reducer;
