import { League } from '@models';

export interface ILeaguesState {
  data: Array<League>;
  loading: boolean;
  selected: {
    data: League | null,
    loading: boolean;
  }
  favorites: {
    data: Array<League>;
    loading: boolean;
  },
  searched: {
    data: Array<League>;
    loading: boolean;
  }
}

export const initialState: ILeaguesState = {
  data: [],
  loading: false,
  selected: {
    data: null,
    loading: false,
  },
  favorites: {
    data: [],
    loading: false,
  },
  searched: {
    data: [],
    loading: false,
  },
};
