import { createAsyncThunk } from '@reduxjs/toolkit';
import { League } from '@models';
import api from './api';

export const getLeague = createAsyncThunk('leagues/getOne', async (value: string): Promise<League> => api.getLeague(value));

export const getAllLeagues = createAsyncThunk('leagues/getAll', async (): Promise<Array<League>> => api.getAll());

export const searchLeagues = createAsyncThunk('leagues/search', async (value: string): Promise<Array<League>> => {
  if (value.length === 0) return api.getAll();
  return api.searchLeagues(value);
});

export const getFavoriteLeagues = createAsyncThunk('leagues/getFavoritesLeagues', async (): Promise<Array<League>> => api.getFavorites());

export const addFavorite = createAsyncThunk('leagues/addFavorite', async (id: string): Promise<Array<League>> => api.addFavorite(id));

export const removeFavorite = createAsyncThunk('leagues/removeFavorite', async (id: string): Promise<Array<League>> => api.deleteFavorite(id));

export const init = createAsyncThunk('leagues/init', async (): Promise<Array<League>> => {
  try {
    return api.getFavorites();
  } catch { return []; }
});
