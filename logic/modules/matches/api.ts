import axios, { AxiosResponse } from 'axios';
import { Match } from '@models';
import { toMatch, RestMatch } from '@models/rest';

const getMatchesFromDate = (value: string): Promise<Array<Match>> => axios.get(`/games/foot/date/${value}`).then(
  (response: AxiosResponse<Array<RestMatch>>) => response.data.map((data) => toMatch(data)),
);

const getMatchesFromTeam = (value: string): Promise<Array<Match>> => axios.get(`/games/foot/team/${value}/previous`).then(
  (response: AxiosResponse<Array<RestMatch>>) => response.data.map((data) => toMatch(data)),
);

const searchMatches = (value: string): Promise<Array<Match>> => axios.get(`/games/foot/search/${value}`).then(
  (response: AxiosResponse<Array<RestMatch>>) => response.data.map((data) => toMatch(data)),
);

interface Rounds {
  previous: string;
  current: string;
  next: string;
}
const getMatchesFromLeague = (leagueId: string, round?: string): Promise<{games: Array<Match>;
  gamesByRound: Array<Match>; rounds: Rounds}> => axios.get(`/games/foot/competition/${leagueId}${round ? `?round=${round}` : ''}`).then(
  (response: AxiosResponse<{games: Array<RestMatch>; gamesByRound: Array<RestMatch>; rounds: Rounds}>) => ({
    games: response.data.games.map((data) => toMatch(data)),
    gamesByRound: response.data.gamesByRound.map((data) => toMatch(data)),
    rounds: response.data.rounds,
  }),
);

const getMatch = (value: string): Promise<Match> => axios.get(`/games/foot/${value}?statistics=true`).then(
  (response: AxiosResponse<RestMatch>) => toMatch(response.data),
);

export default {
  getMatchesFromDate, getMatchesFromTeam, getMatchesFromLeague, getMatch, searchMatches,
};
