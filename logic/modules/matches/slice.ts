import { createSlice } from '@reduxjs/toolkit';
import { initialState, IMatchesState } from './state';
import {
  getMatches, getMatchesFromDate, getMatch, searchMatches,
} from './thunks';

const matchesSlice = createSlice({
  name: 'matches',
  initialState,
  reducers: {
    setMatches: (state, { payload }): IMatchesState => ({ ...state, data: payload }),
    setMatch: (state, { payload }): IMatchesState => ({ ...state, selected: { data: payload, loading: false } }),
  },
  extraReducers: (builder) => {
    // when getAllMatches request starts, execute reducer
    builder.addCase(getMatches.pending, (state) => ({ ...state, loading: true }));
    builder.addCase(getMatches.rejected, (state) => ({ ...state, loading: false }));
    builder.addCase(getMatches.fulfilled, (state, { payload }) => ({ ...state, data: payload, loading: false }));
    // when getMatchesFromDate request starts, execute reducer
    builder.addCase(getMatchesFromDate.pending, (state) => ({ ...state, loading: true }));
    builder.addCase(getMatchesFromDate.rejected, (state) => ({ ...state, loading: false }));
    builder.addCase(getMatchesFromDate.fulfilled, (state, { payload }) => ({ ...state, data: payload, loading: false }));
    // when searchMatches request starts, execute reducer
    builder.addCase(searchMatches.pending, (state) => ({ ...state, loading: true }));
    builder.addCase(searchMatches.rejected, (state) => ({ ...state, loading: false }));
    builder.addCase(searchMatches.fulfilled, (state, { payload }) => ({ ...state, data: payload, loading: false }));
    // when getMatch request starts, execute reducer
    builder.addCase(getMatch.pending, (state) => ({ ...state, selected: { ...initialState.selected, loading: true } }));
    builder.addCase(getMatch.rejected, (state) => ({ ...state, selected: { ...state.selected, loading: true } }));
    builder.addCase(getMatch.fulfilled, (state, { payload }) => ({
      ...state,
      selected: { ...state.selected, data: { ...state.selected.data, ...payload }, loading: false },
    }));
  },
});

export const matchesActions = {
  ...matchesSlice.actions, getMatches, getMatchesFromDate, getMatch, searchMatches,
};
export const matchesReducer = matchesSlice.reducer;
