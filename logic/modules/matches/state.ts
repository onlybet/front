import { Match } from '@models';

export interface IMatchesState {
  selected: {
    data: Match | null;
    loading: boolean;
  };
  data: Array<Match>;
  loading: boolean;
}

export const initialState: IMatchesState = {
  data: [],
  selected: {
    data: null,
    loading: false,
  },
  loading: false,
};
