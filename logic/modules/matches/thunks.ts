import { createAsyncThunk } from '@reduxjs/toolkit';
import { Match } from '@models';
import { fakeCall } from '@logic/utils';
import api from './api';

export const getMatches = createAsyncThunk('matches/getMatches', async (LeagueID: string): Promise<Array<Match>> => fakeCall({
  stall: 500,
  response: [],
}));

export const getMatchesFromDate = createAsyncThunk('matches/getMatchesFromDate', async (date: string): Promise<Array<Match>> => api.getMatchesFromDate(date));
export const getMatchesFromTeam = createAsyncThunk('matches/getMatchesFromTeam', async (teamId: string): Promise<Array<Match>> => api.getMatchesFromTeam(teamId));
export const getMatch = createAsyncThunk('matches/getMatch', async (teamId: string): Promise<Match> => api.getMatch(teamId));
export const searchMatches = createAsyncThunk('matches/search', async (value: string): Promise<Array<Match>> => api.searchMatches(value));
