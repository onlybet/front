import axios, { AxiosResponse } from 'axios';
import { SelfUser } from '@models';
import { toSelfUser, RestSelfUser } from '@models/rest';

const login = (email: string, password: string): Promise<SelfUser> => axios.post('/user/sign-in', {
  email,
  password,
}).then((response: AxiosResponse<RestSelfUser>) => toSelfUser(response.data));

const signUp = (email: string, password: string): Promise<SelfUser> => axios.post('/user/sign-up', {
  email,
  password,
}).then((response: AxiosResponse<RestSelfUser>) => toSelfUser(response.data));

const init = (token: string): Promise<SelfUser> => axios.get('/user/init', {
  headers: {
    Authorization: token,
  },
}).then((response: AxiosResponse<RestSelfUser>) => toSelfUser(response.data));

export default { login, signUp, init };
