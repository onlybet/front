import { createSlice } from '@reduxjs/toolkit';
import { initialState, IUserState } from './state';
import { setGlobalApiAuthorizationToken } from '../../api';
import { Keys, Storage } from '../../store/storage';
import { login, signUp, init } from './thunks';

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUser: (state, { payload }): IUserState => ({ ...state, data: payload }),
    signOut: (): IUserState => {
      Storage.remove(Keys.token);
      return initialState;
    },
  },
  extraReducers: (builder) => {
    // when login request starts, execute reducer
    builder.addCase(login.pending, (state) => ({ ...state, loading: true }));
    // case login successful, set authorization token on api
    // and save token in storage
    builder.addCase(login.fulfilled, (state, { payload }) => {
      setGlobalApiAuthorizationToken(payload.token);
      Storage.set(Keys.token, payload.token);
      return { ...state, data: payload, loading: false };
    });
    builder.addCase(login.rejected, (state) => ({ ...state, error: 'No user found', loading: false }));
    // when signUp request starts, execute reducer
    builder.addCase(signUp.pending, (state) => ({ ...state, loading: true }));
    // case signUp successful, set authorization token on api
    // and save token in storage
    builder.addCase(signUp.fulfilled, (state, { payload }) => {
      setGlobalApiAuthorizationToken(payload.token);
      Storage.set(Keys.token, payload.token);
      return { ...state, data: payload, loading: false };
    });
    builder.addCase(signUp.rejected, (state) => ({ ...state, error: 'User already exist', loading: false }));
    // when init request starts, execute reducer
    builder.addCase(init.pending, (state) => ({ ...state, loading: true }));
    // case init successful, set authorization token on api
    // and save token in storage
    builder.addCase(init.fulfilled, (state, { payload }) => {
      if (payload) {
        setGlobalApiAuthorizationToken(payload.token);
        Storage.set(Keys.token, payload.token);
      }
      return { ...state, data: payload || null, loading: false };
    });
    builder.addCase(init.rejected, (state) => ({ ...state, data: null, loading: false }));
  },
});

export const userActions = {
  ...userSlice.actions, login, signUp, init,
};
export const userReducer = userSlice.reducer;
