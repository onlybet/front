import { RestUser } from '@models/rest';

export interface IUserState{
  data: RestUser | null;
  error: string;
  loading: boolean;
}

export const initialState: IUserState = {
  data: null,
  error: '',
  loading: false,
};
