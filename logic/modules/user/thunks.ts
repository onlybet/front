import { createAsyncThunk } from '@reduxjs/toolkit';
import { SelfUser } from '@models';
import { Storage, Keys } from '@logic/store/storage';
import api from './api';

interface ICredentials { email: string; password: string }
export const login = createAsyncThunk('user/login', async (data: ICredentials): Promise<SelfUser> => api.login(data.email, data.password));

export const signUp = createAsyncThunk('user/signUp', async (data: ICredentials): Promise<SelfUser> => api.signUp(data.email, data.password));

export const init = createAsyncThunk('user/init', async (): Promise<SelfUser | undefined> => {
  try {
    const token = await Storage.get(Keys.token);
    return api.init(token);
  } catch { return undefined; }
});

/*
import { loginAPI } from '../../api/user';
export const login = createAsyncThunk('user/login', async (data: ILogin, { rejectWithValue }) => loginAPI(data.email, data.password)
  .catch((e) => rejectWithValue(e)));
*/
