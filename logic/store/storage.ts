import AsyncStorage from '@react-native-async-storage/async-storage';

export const enum Keys {
  token = 'token',
  user = 'user'
}

type ObjectType<T> =
  T extends Keys.token ? string :
  T extends Keys.user ? string :
  never;

export class Storage {
  static get = async <T extends Keys>(key: T): Promise<ObjectType<T>> => {
    const data = await AsyncStorage.getItem(key);
    if (data) return JSON.parse(data);
    throw new Error(`nothing found with key: ${key}`);
  };

  static set = async <T extends Keys>(key: T, value: ObjectType<T>): Promise<void> => {
    await AsyncStorage.setItem(key, JSON.stringify(value));
  };

  static remove = async (key: Keys): Promise<void> => {
    await AsyncStorage.removeItem(key);
  };
}
