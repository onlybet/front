import { configureStore } from '@reduxjs/toolkit';
import { userActions, userReducer } from '../modules/user/slice';
import { leaguesActions, leaguesReducer } from '../modules/leagues/slice';
import { matchesActions, matchesReducer } from '../modules/matches/slice';

const combinedReducers = { user: userReducer, leagues: leaguesReducer, matches: matchesReducer };

export const store = configureStore({ reducer: { ...combinedReducers } });

export const combinedActions = { user: userActions, leagues: leaguesActions, matches: matchesActions };

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
