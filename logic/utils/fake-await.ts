interface IFakeCall<T> {
  stall?: number;
  response?: T;
}

export const fakeCall = async <P>(data: IFakeCall<P>): Promise<P> => {
  const finalData: IFakeCall<P> = { stall: 1000, response: undefined, ...data };
  await new Promise((resolve) => setTimeout(resolve, finalData.stall));
  return finalData.response as P;
};
