import type { Match } from './match';

export interface League {
  id: string;
  name: string;
  region: string;
  imageURI: string;
  caption: string;
  matches: Array<Omit<Match, 'league'>>;
  active: boolean;
}
