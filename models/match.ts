import type { League } from './league';
import { Team } from './team';

export interface MatchStatistic {
  name: string;
  value: number;
}

export interface MatchTeam extends Team {
  statistics: Array<MatchStatistic>;
  points: number;
}

export interface Match {
  id: string;
  teams: Array<MatchTeam>;
  durationTime: number;
  date: Date;
  league: Omit<League, 'matches'>;
}
