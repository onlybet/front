import { League } from '@models';
import type { RestMatch } from './match';
// eslint-disable-next-line import/no-cycle
import { toMatch } from './match';

export interface RestLeague {
  _id: string;
  name: string;
  country: string;
  logo: string;
  caption: string;
  matches: Array<Omit<RestMatch, 'league'>>;
  active: boolean;
}

export const toLeague = (restLeague: RestLeague): League => ({
  id: restLeague._id,
  name: restLeague.name,
  region: restLeague.country,
  imageURI: restLeague.logo,
  caption: restLeague.caption || '',
  matches: restLeague.matches?.map((match) => toMatch(match)) || [],
  active: restLeague.active,
});
