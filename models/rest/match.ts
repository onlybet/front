/* eslint-disable @typescript-eslint/no-explicit-any */
import { Match, MatchStatistic, MatchTeam } from '@models';
// eslint-disable-next-line import/no-cycle
import { RestLeague, toLeague } from './league';
import { RestTeam, toTeam } from './team';

interface RestMatchStatistic {
  name: string;
  value: number | string | null;
}

const toMatchStatistic = (restMatchStatistic: RestMatchStatistic): MatchStatistic => ({
  name: restMatchStatistic.name,
  value: Number(restMatchStatistic.value) ? Number(restMatchStatistic.value) : parseFloat(restMatchStatistic.value?.toString() || '0'),
});

const toMatchTeam = (restMatchTeam: RestTeam, points: number, statistics: object): MatchTeam => ({
  ...toTeam(restMatchTeam),
  statistics: Object.keys(statistics).map((key) => toMatchStatistic({ name: key, value: (<any>statistics)[key] })),
  points,
});

export interface RestMatch {
  _id: string;
  teams: {
    teamHome: RestTeam;
    teamAway: RestTeam;
  };
  statistics?: {
    teamHome: object;
    teamAway: object;
  };
  durationTime: number;
  date: Date;
  competition: Omit<RestLeague, 'matches'>;
  goals: {
    goalsTeamHome: number | null;
    goalsTeamAway: number | null;
  }
}

export const toMatch = (restMatch: RestMatch): Match => ({
  id: restMatch._id,
  teams: [toMatchTeam(restMatch.teams.teamHome, restMatch.goals.goalsTeamHome || 0, restMatch.statistics ? restMatch.statistics.teamHome : {}),
    toMatchTeam(restMatch.teams.teamAway, restMatch.goals.goalsTeamAway || 0, restMatch.statistics ? restMatch.statistics.teamAway : {})],
  durationTime: 0,
  date: restMatch.date,
  league: toLeague({ ...restMatch.competition, matches: [] }),
});
