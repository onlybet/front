import { Team } from '@models';

export interface RestTeam {
  _id: string;
  name: string;
  shortedName: string;
  logo: string;
}

export const toTeam = (restTeam: RestTeam): Team => ({
  id: restTeam._id,
  name: restTeam.name,
  shortedName: restTeam.shortedName,
  imageURI: restTeam.logo,
});
