import { SelfUser, User } from '../user';

export interface RestUser {
  id: string;
  email: string;
}

export interface RestSelfUser extends RestUser {
  token: string;
  favorites:{
    competitions: Array<number>;
  }
}

export const toUser = (restUser: RestUser): User => ({
  id: restUser.id,
  email: restUser.email,
});

export const toSelfUser = (restUser: RestSelfUser): SelfUser => ({
  id: restUser.id,
  email: restUser.email,
  token: restUser.token,
});
