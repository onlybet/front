export interface Team {
  id: string;
  name: string;
  shortedName: string;
  imageURI: string;
}
