export interface User {
  id: string;
  email: string;
}

export interface SelfUser extends User {
  token: string;
}
