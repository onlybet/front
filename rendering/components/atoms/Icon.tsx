/* eslint-disable react/require-default-props */
/* eslint-disable react/default-props-match-prop-types */
/* eslint-disable react/display-name */
import {
  Feather, MaterialCommunityIcons, Ionicons,
} from '@expo/vector-icons';
import { Icon as NbIcon } from 'native-base';
import React from 'react';

// get props from font library
export type IFeatherProps = React.ComponentProps<typeof Feather>;
export type IMaterialCommunityIconsProps = React.ComponentProps<typeof MaterialCommunityIcons>;
export type IIoniconsProps = React.ComponentProps<typeof Ionicons>;
type INbIconProps = React.ComponentProps<typeof NbIcon>;

// generate template interface
export interface IIconProps<T extends IFeatherProps | IMaterialCommunityIconsProps | IIoniconsProps> extends INbIconProps{
  name?: T['name'];
  size: T['size'];
}

// generate icon templates
const IoniconsIcon = (props: IIconProps<IIoniconsProps>) => <NbIcon as={<Ionicons name={props.name} size={props.size} />} {...props} />;
const FeatherIcon = (props: IIconProps<IFeatherProps>) => <NbIcon as={<Feather name={props.name} size={props.size} />} {...props} />;

// generate base component
export function Icon(props: IIconProps<IFeatherProps>) { return <FeatherIcon {...props} />; }

const ChevronLeft = ({ ...props }: IIconProps<IFeatherProps>) => <FeatherIcon {...props} />;
ChevronLeft.defaultProps = { name: 'chevron-left' };

const ChevronRight = ({ ...props }: IIconProps<IFeatherProps>) => <FeatherIcon {...props} />;
ChevronRight.defaultProps = { name: 'chevron-right' };

const Search = ({ ...props }: IIconProps<IFeatherProps>) => <FeatherIcon {...props} />;
Search.defaultProps = { name: 'search' };

const ArrowLeft = ({ ...props }: IIconProps<IFeatherProps>) => <FeatherIcon {...props} />;
ArrowLeft.defaultProps = { name: 'arrow-left' };

const StarOutlined = ({ ...props }: IIconProps<IIoniconsProps>) => <IoniconsIcon {...props} />;
StarOutlined.defaultProps = { name: 'star-outline' };

const StarFilled = ({ ...props }: IIconProps<IIoniconsProps>) => <IoniconsIcon {...props} />;
StarFilled.defaultProps = { name: 'star' };

const Home = ({ ...props }: IMaterialCommunityIconsProps) => <MaterialCommunityIcons {...props} />;
Home.defaultProps = { name: 'home' };

const Loader = ({ ...props }: IIconProps<IFeatherProps>) => <FeatherIcon {...props} />;
Loader.defaultProps = { name: 'loader' };

// generate child components
Icon.ChevronLeft = ChevronLeft;
Icon.ChevronRight = ChevronRight;
Icon.Search = Search;
Icon.ArrowLeft = ArrowLeft;
Icon.Home = Home;
Icon.StarOutlined = StarOutlined;
Icon.StarFilled = StarFilled;
Icon.Loader = Loader;
