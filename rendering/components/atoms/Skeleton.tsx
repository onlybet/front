import React from 'react';
import { Skeleton as NBSkeleton, ISkeletonProps, useColorModeValue } from 'native-base';

export const Skeleton = (props: ISkeletonProps) => (
  <NBSkeleton
    startColor={useColorModeValue('light.800', 'dark.800')}
    endColor={useColorModeValue('light.900', 'dark.900')}
    {...props}
  />
);
