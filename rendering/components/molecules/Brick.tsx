import React from 'react';
import {
  Flex, VStack, Avatar, useColorModeValue, useBreakpointValue, IFlexProps, Pressable, IButtonProps,
} from 'native-base';
import { Text, Skeleton } from '../atoms';

interface Image {
  src: string;
  active: boolean;
}
export interface IBrickProps extends IButtonProps{
  images: Array<Image>;
  title: string;
  subtitle: string;
  caption: string;
}

const Avatars = ({ images }: {images: Array<Image>}) => (
  <VStack>
    <Avatar
      bg="transparent"
      source={{ uri: images[0].src }}
      size={images.length > 1 ? ['14px', null, null, '24px'] : ['8', null, null, '10']}
    >
      {images[0].active && images.length < 2 && (
      <Avatar.Badge
        bg="green.500"
        borderColor={useColorModeValue('light.900', 'dark.900')}
      />
      )}
    </Avatar>
    {images[1] && (
    <Avatar
      bg="transparent"
      size={images.length > 1 ? '14px' : '8'}
      source={{ uri: images[1].src }}
    />
    )}
  </VStack>
);

const BrickSkeleton = (props: IFlexProps) => (
  <Flex
    direction="row"
    align="center"
    justify="space-between"
    bg={useColorModeValue('light.900', 'dark.900')}
    p="4"
    borderRadius={8}
    {...props}
  >
    <Flex w="80%" direction="row" align="center">
      <Skeleton
        {...useBreakpointValue({
          base: {
            w: '8',
            h: '8',
          },
          lg: {
            w: '10',
            h: '10',
          },
        })}
        borderRadius="full"
      />
      <VStack w="60%" ml="2" space="1">
        <Skeleton height={useBreakpointValue({ base: 3, lg: 4 })} key="1" w="100%" />
        <Skeleton height="2" key="2" w="60%" />
      </VStack>
    </Flex>
    <Skeleton height="4" key="1" w="10%" />
  </Flex>
);

const Brick = ({
  images, title, subtitle, caption, w, onPress, ...props
}: IBrickProps) => (
  <Pressable
    p="4"
    bg={useColorModeValue('light.900', 'dark.900')}
    borderRadius={8}
    w={w}
    onPress={onPress}
    {...props}
  >
    <Flex
      direction="row"
      align="center"
      justify="space-between"
      w="100%"
    >
      <Flex direction="row" alignItems="center">
        {images.length > 0 && (
          <Avatars images={images} />
        )}
        <VStack ml={images.length > 0 ? '2' : '0'}>
          <Text
            size="md"
            fontWeight="bold"
            color={useColorModeValue('light.100', 'dark.100')}
          >
            {title}
          </Text>
          <Text size="sm" color={useColorModeValue('light.100', 'dark.100')}>{subtitle}</Text>
        </VStack>
      </Flex>
      <Text size="lg" color={useColorModeValue('light.700', 'dark.700')} ml="4">{caption}</Text>
    </Flex>
  </Pressable>
);

Brick.Skeleton = BrickSkeleton;
export { Brick };
