import React from 'react';
import {
  Pressable, Flex, Box, VStack, HStack, useColorModeValue, IPressableProps, useBreakpointValue, Image, IBoxProps,
} from 'native-base';
import { Text, Skeleton } from '../atoms';

type status = 'active' | 'inactive' | 'hidden';
interface ICardProps extends IPressableProps {
  upperChildren: {
    node: React.ReactNode;
    status: status;
  }
  images: {
    leftSrc: string;
    rightSrc: string;
  }
  bottomChildren: {node: React.ReactNode;}
}

// default card component
export const Card = ({
  upperChildren, images, bottomChildren, ...props
} : ICardProps) => (
  <Pressable
    bg="primary.400"
    py="4"
    px={useBreakpointValue({ base: 2, lg: 4 })}
    borderRadius="8"
    display="flex"
    flexDirection="column"
    justifyContent="space-between"
    {...props}
  >
    <Flex direction="row" align="flex-start" justify="space-between">
      {upperChildren.node}
      {upperChildren.status !== 'hidden' && <Box bg="light.900" w="2" h="2" borderRadius="full" />}
    </Flex>
    <HStack space={useBreakpointValue({ base: 3, lg: 4 })} my={useBreakpointValue({ base: 6, lg: 8 })} mx={useBreakpointValue({ base: 2, lg: 0 })}>
      <Image
        size={useBreakpointValue({ base: 10, lg: 16 })}
        source={{ uri: images.leftSrc }}
      />
      <Image
        size={useBreakpointValue({ base: 10, lg: 16 })}
        source={{ uri: images.rightSrc }}
      />
    </HStack>
    {bottomChildren.node}
  </Pressable>
);

interface Team {
  name: string;
  score: number;
  imageSrc: string;
}
interface IFootballProps extends IPressableProps{
  information: string;
  date: Date;
  status: status;
  teams: Array<Team>;
}

// loading skeleton
const CardSkeleton = ({ ...props }: IBoxProps) => (
  <Box
    px={useBreakpointValue({ base: 2, lg: 4 })}
    bg={useColorModeValue('light.900', 'dark.900')}
    py="4"
    borderRadius={8}
    {...props}
  >
    <VStack space={1}>
      <Skeleton height={useBreakpointValue({ base: 3, lg: 4 })} key="1" w={useBreakpointValue({ base: 24, lg: 32 })} />
      <Skeleton height={useBreakpointValue({ base: 2, lg: 3 })} key="2" w={useBreakpointValue({ base: 16, lg: 20 })} />
    </VStack>
    <HStack space={useBreakpointValue({ base: 3, lg: 4 })} my={useBreakpointValue({ base: 6, lg: 8 })} mx={useBreakpointValue({ base: 2, lg: 0 })}>
      <Skeleton size={useBreakpointValue({ base: 10, lg: 16 })} borderRadius={6} key="7" />
      <Skeleton size={useBreakpointValue({ base: 10, lg: 16 })} borderRadius={6} key="8" />
    </HStack>
    <VStack space={1}>
      <Flex direction="row" align="flex-start" justify="space-between">
        <Skeleton height={useBreakpointValue({ base: 2, lg: 3 })} key="3" w={useBreakpointValue({ base: 16, lg: 20 })} />
        <Skeleton height={useBreakpointValue({ base: 2, lg: 3 })} key="4" w={useBreakpointValue({ base: 2, lg: 3 })} />
      </Flex>
      <Flex direction="row" align="flex-start" justify="space-between">
        <Skeleton height={useBreakpointValue({ base: 2, lg: 3 })} key="5" w={useBreakpointValue({ base: 20, lg: 24 })} />
        <Skeleton height={useBreakpointValue({ base: 2, lg: 3 })} key="6" w={useBreakpointValue({ base: 2, lg: 3 })} />
      </Flex>
    </VStack>
  </Box>
);

// customized card for Football
const Football = ({
  date, status, teams, information, ...props
}: IFootballProps) => {
  if (teams.length < 2) return <></>;
  return (
    <Card
      upperChildren={{
        node: (
          <VStack>
            <HStack>
              <Text
                size="xs"
                fontWeight="bold"
                color={useColorModeValue('primary.content', 'primary.content')}
                mr="1"
              >
                {String(date.getDate()).padStart(2, '0')}
                /
                {String(date.getMonth() + 1).padStart(2, '0')}
              </Text>
              <Text size="xs" color={useColorModeValue('primary.content', 'primary.content')}>
                {date.getUTCHours()}
                :
                {date.getUTCMinutes()}
                {' UTC'}
              </Text>
            </HStack>
            <HStack>
              <Text size="xs" color={useColorModeValue('primary.content', 'primary.content')}>{information}</Text>
            </HStack>
          </VStack>
        ),
        status,
      }}
      images={{
        leftSrc: teams[0].imageSrc,
        rightSrc: teams[1].imageSrc,
      }}
      bottomChildren={{
        node: (
          <VStack>
            {teams.map((team) => (
              <Flex direction="row" align="center" justify="space-between" key={team.name}>
                <Text size="xs" color={useColorModeValue('primary.content', 'primary.content')} mr="1">{team.name}</Text>
                <Text size="sm" color={useColorModeValue('primary.content', 'primary.content')}>{team.score}</Text>
              </Flex>
            ))}
          </VStack>
        ),
      }}
      {...props}
    />
  );
};

Card.Football = Football;
Card.Skeleton = CardSkeleton;
