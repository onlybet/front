import React from 'react';
import {
  VStack, Heading, Divider, Flex, useColorModeValue, Pressable,
} from 'native-base';
import { Icon } from '../atoms';

type IVStackProps = React.ComponentProps<typeof VStack>;

interface ICarouselProps extends IVStackProps {
  text: string;
  icon?: string;
  showIcon?: boolean;
  onPressLeft?: () => void;
  onPressRight?: () => void;
}

export const Carousel = ({
  onPressLeft, onPressRight, text, showIcon, icon, ...props
}: ICarouselProps) => (
  <VStack w="100%" {...props}>
    <Flex direction="row" justify="space-between" align="center" w="100%">
      <Pressable
        bg="transparent"
        p="0"
        onPress={() => onPressLeft && onPressLeft()}
      >
        <Icon.ChevronLeft
          size={8}
        />
      </Pressable>
      <Divider
        orientation="horizontal"
        thickness="1"
        w="15%"
        bg={useColorModeValue('light.700', 'dark.700')}
      />
      <Flex direction="row" justify="center" align="center" pl="3" pr="3" maxW="50%">
        {/* eslint-disable-next-line @typescript-eslint/no-explicit-any */}
        {(showIcon || text === '') && <Icon name={icon as any} size={28} color={useColorModeValue('light.100', 'dark.100')} />}
        {text !== '' && <Heading ml="2" textAlign="center" color={useColorModeValue('light.100', 'dark.100')}>{text}</Heading>}
      </Flex>
      <Divider
        orientation="horizontal"
        thickness="1"
        w="15%"
        bg={useColorModeValue('light.700', 'dark.700')}
      />
      <Pressable
        onPress={() => onPressRight && onPressRight()}
      >
        <Icon.ChevronRight
          size={8}
        />
      </Pressable>
    </Flex>
  </VStack>
);

Carousel.defaultProps = {
  icon: 'calendar',
  showIcon: true,
  onPressLeft: undefined,
  onPressRight: undefined,
};
