import React from 'react';
import {
  Flex, useColorModeValue, VStack, HStack,
} from 'native-base';
import { Progress, Text } from '../atoms';

export interface IDualProgress {
  value1:number,
  value2:number,
  title:string
}

export const pourcent = (value: number, total: number) => (value / total) * 100;

export const DualProgress = (props: IDualProgress) => {
  const total = props.value1 + props.value2;
  return (
    <VStack {...props} space="4px" width="100%">
      <Flex justifyContent="space-between" direction="row">
        <Text
          size="md"
          color={useColorModeValue('light.50', 'dark.50')}
        >
          {props.value1}
        </Text>
        <Text
          size="md"
          color={useColorModeValue('light.50', 'dark.50')}
        >
          {props.title}
        </Text>
        <Text
          size="md"
          color={useColorModeValue('light.50', 'dark.50')}
        >
          {props.value2}
        </Text>
      </Flex>
      <HStack>
        <Progress
          height="1"
          borderRadius={0}
          colorScheme="primary"
          bg={useColorModeValue('light.800', 'dark.800')}
          value={pourcent(props.value1, total)}
          w="50%"
          style={{ transform: [{ scaleX: -1 }] }}
        />
        <Progress
          height="1"
          borderRadius={0}
          colorScheme="amber"
          bg={useColorModeValue('light.800', 'dark.800')}
          value={pourcent(props.value2, total)}
          w="50%"
        />
      </HStack>
    </VStack>
  );
};
