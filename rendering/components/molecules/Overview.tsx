import {
  Flex, Center, Image, IFlexProps,
} from 'native-base';
import React from 'react';
import { Text } from '../atoms';

export interface OverviewProps extends IFlexProps{
  teams: Array<{
    name: string;
    imageSrc: string;
    score: number; //
  }>;
  duration?: string;
}

export const Overview = ({ teams, duration, ...props }: OverviewProps) => (
  <Flex align="center" justify="space-between" direction="row" {...props}>
    <Image
      size="24"
      source={{
        uri: teams[0].imageSrc,
      }}
      alt={teams[0].name}
    />
    <Center>
      <Text size="3xl">
        {teams[0].score}
        {' '}
        -
        {' '}
        {teams[1].score}
      </Text>
      {duration && duration !== '0' && <Text size="lg">{duration}</Text>}
    </Center>
    <Image
      size="24"
      source={{
        uri: teams[1].imageSrc,
      }}
      alt={teams[1].name}
    />
  </Flex>
);
Overview.defaultProps = { duration: undefined };
