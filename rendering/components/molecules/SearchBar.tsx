import React, { forwardRef } from 'react';
import {
  HStack, useBreakpointValue, useColorModeValue, Pressable, IPressableProps,
} from 'native-base';
import { Icon, Input } from '../atoms';

export interface ISearchBar extends IPressableProps{
  placeholder: string;
  isReadOnly: boolean;
  isDisabled?: boolean;
  onChange?: (value: string) => void;
}

// eslint-disable-next-line react/display-name
const SearchBar = forwardRef(({
  placeholder, isDisabled, isReadOnly, onPress, onChange, ...props
}: ISearchBar, ref) => (
  <Pressable
    onPress={onPress}
    {...props}
  >
    <HStack
      alignItems="center"
      bg={useColorModeValue('light.900', 'dark.900')}
      borderRadius="8"
      pl={useBreakpointValue({ base: 4, lg: 8 })}
      py={useBreakpointValue({ base: 2, lg: 4 })}
      space={useBreakpointValue({ base: 2, lg: 6 })}
    >
      <Icon.Search
        size={6}
        color={useColorModeValue('light.600', 'dark.600')}
      />
      <Input
        placeholder={placeholder}
        _hover={{ bg: 'transparent' }}
        width="100%"
        height="100%"
        placeholderTextColor={useColorModeValue('light.600', 'dark.600')}
        borderWidth="0"
        isDisabled={isDisabled}
        isReadOnly={isReadOnly}
        color={useColorModeValue('light.100', 'dark.100')}
        pr={useBreakpointValue({ base: 4, lg: 8 })}
        size="md"
        onChange={(e) => {
          e.persist();
          if (onChange) {
            onChange(((e.target) as any).value);
          }
        }}
        ref={ref}
      />
    </HStack>
  </Pressable>
));
SearchBar.defaultProps = {
  isDisabled: false,
  isReadOnly: false,
  onChange: undefined,
};

export { SearchBar };
