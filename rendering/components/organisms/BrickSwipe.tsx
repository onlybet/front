import Swipeable from 'react-native-gesture-handler/Swipeable';
import React from 'react';
import {
  TouchableOpacity,
} from 'react-native';
import { Center, useColorModeValue } from 'native-base';
import { Brick } from '../molecules';
import { Icon } from '../atoms';

type BrickProps = React.ComponentProps<typeof Brick>;

interface BrickSwipeProps extends BrickProps {
  onSwipeAction: () => void;
  isFavorite: boolean;
}

export const BrickSwipe = ({ onSwipeAction, isFavorite, ...props }: BrickSwipeProps) => {
  const bgColor = useColorModeValue('primary.500', 'primary.500');
  const color = useColorModeValue('primary.content', 'primary.content');
  const swipe = () => (
    <TouchableOpacity onPress={onSwipeAction} activeOpacity={0.6}>
      <Center h="100%" w="20" px="4" bg={bgColor} ml="4" borderRadius="8">
        {isFavorite ? <Icon.StarFilled size={6} color={color} /> : <Icon.StarOutlined size={6} color={color} />}
        {/* <Text color={color} textAlign="center" size="sm" mt="1">{isFavorite ? 'Remove from favorite' : 'Add to favorite'}</Text> */}
      </Center>
    </TouchableOpacity>
  );
  return (
    <Swipeable renderRightActions={swipe}>
      <Brick {...props} />
    </Swipeable>
  );
};

BrickSwipe.Skeleton = Brick.Skeleton;
