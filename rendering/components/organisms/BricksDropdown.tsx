import React, { useState } from 'react';
import {
  VStack, Flex, Box, useColorModeValue, IFlexProps,
} from 'native-base';
import { Brick, IBrickProps } from '../molecules';

interface IBrickChildProps extends IBrickProps {
  id: string;
}
interface IBricksDropdownProps {
  parent: IBrickProps;
  list: Array<IBrickChildProps>;
}
export const BricksDropdown = ({ parent, list }: IBricksDropdownProps) => {
  const [isOpen, setIsOpen] = useState(false);
  const color = useColorModeValue('light.900', 'dark.900');

  return (
    <Box
      w="full"
      p={isOpen ? '3' : '0'}
      borderRadius={8}
      bg={isOpen ? 'primary.500' : '0'}
    >
      <Brick
        w="100%"
        onPress={() => setIsOpen(!isOpen)}
        {...parent}
      />
      {isOpen && (
      <Flex w="100%" direction="row" justify="flex-end">
        <Box
          h="95%"
          w="1"
          bg={{
            linearGradient: {
              colors: [color, 'transparent'],
              start: [0, 0],
              end: [0, 1],
            },
          }}
          ml="2"
          mr="3"
          mt="-1px"
        />
        <VStack w="89%" space="2" mt="2">
          {list.map((element) => <Brick w="full" {...element} key={element.id} />)}
        </VStack>
      </Flex>
      )}
    </Box>
  );
};

export const BricksDropdownSkeleton = (props: IFlexProps) => (
  <Brick.Skeleton {...props} />
);

BricksDropdown.Skeleton = BricksDropdownSkeleton;
