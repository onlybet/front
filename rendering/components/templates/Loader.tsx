import { Center, ICenterProps, Spinner } from 'native-base';
import React from 'react';

export const Loader = ({ ...props }: ICenterProps) => (
  <Center {...props}>
    <Spinner accessibilityLabel="Loading posts" color="primary.500" />
  </Center>
);
