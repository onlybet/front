import React from 'react';
import { Box, ScrollView as SV, IScrollViewProps } from 'native-base';

const Background = () => (
  <Box
    position="absolute"
    h="100%"
    w="100%"
    _dark={{
      bg: {
        linearGradient: {
          colors: ['#000000', '#131313', '#131313'],
          start: [0, 0],
          end: [1, 1],
        },
      },
    }}
    _light={{
      bg: {
        linearGradient: {
          colors: ['#FFFFFF', '#F9F9F9', '#F9F9F9'],
          start: [0, 0],
          end: [1, 1],
        },
      },
    }}
  />
);

export const ScrollView = ({ children, bg, ...props }:IScrollViewProps) => {
  if (bg === 'none') {
    return (
      <SV {...props}>
        {children}
      </SV>
    );
  }

  return (
    <>
      <Background />
      <SV {...props}>
        {children}
      </SV>
    </>
  );
};
