import React from 'react';
import {
  NativeBaseProvider,
  extendTheme,
  Box,
} from 'native-base';

import AppLoading from 'expo-app-loading';
import { theme, loadFonts } from '@theme';
import { StoreProvider } from '@contexts';
import { initAxios } from '@api';
import { Router } from './Router';

initAxios();

const config = {
  dependencies: {
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    'linear-gradient': require('expo-linear-gradient').LinearGradient,
  },
};

// extend the theme
export const themeTemplate = extendTheme({ ...theme });

// 2. Get the type of the CustomTheme
type CustomThemeType = typeof themeTemplate;

// 3. Extend the internal NativeBase Theme
declare module 'native-base' {
  type ICustomTheme = CustomThemeType
}

export default function App() {
  const [fontsLoaded] = loadFonts();
  if (!fontsLoaded) {
    return (
      <AppLoading />
    );
  }
  return (
    <StoreProvider>
      <NativeBaseProvider theme={themeTemplate} config={config}>
        <Box
          h="100%"
          w="100%"
          _dark={{ bg: 'black' }}
          _light={{ bg: 'white' }}
        >
          <Router />
        </Box>
      </NativeBaseProvider>
    </StoreProvider>
  );
}
