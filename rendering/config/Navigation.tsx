import { NativeStackScreenProps } from '@react-navigation/native-stack';

type RootStackParamList = {
  Root: undefined;
  League: { id: string };
  Match: { id: string };
  Search: undefined;
  Profile: undefined;
  Login: undefined;
};

export const routes = {
  screens: {
    Test: 'test',
    Profile: 'profile',
    Match: 'match/:id',
    League: 'league/:id',
    Search: 'search',
    Login: 'login',
    Root: '/',
  },
};

export type DefaultRouteProps<T extends keyof RootStackParamList> = NativeStackScreenProps<RootStackParamList, T>;
