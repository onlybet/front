import React, { useEffect, useState } from 'react';
import { DefaultTheme, DarkTheme, NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {
  Home, Profile, Search, Login, MatchView, League,
} from '@views';
import * as Linking from 'expo-linking';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Icon, Loader } from '@components';
import { Avatar, useColorModeValue } from 'native-base';
import { theme } from '@theme';
import { useStore } from '@contexts';
import { routes } from './Navigation';

const prefix = Linking.createURL('/');

const linking = {
  prefixes: [prefix],
  config: routes,
};

const Tab = createBottomTabNavigator();
const MainStackScreen = () => (
  <Tab.Navigator
    screenOptions={{
      tabBarHideOnKeyboard: true,
      tabBarStyle: {
        backgroundColor: useColorModeValue('white', 'black'),
        borderTopColor: 'transparent',
      },
      headerShown: false,
    }}

  >
    <Tab.Screen
      name="Home"
      component={Home}
      options={{
        tabBarShowLabel: false,
        tabBarIcon: ({ focused }) => (
          <Icon.Home
            size={24}
            color={focused ? theme.colors.primary[500] : theme.colors.light[800]}
          />
        ),
      }}
    />
    <Tab.Screen
      name="Profile"
      component={Profile}
      options={{
        tabBarShowLabel: false,
        tabBarIcon: ({ focused }) => (
          <Avatar
            size="6"
            bg={focused ? theme.colors.primary[500] : theme.colors.light[800]}
          />
        ),
      }}
    />
  </Tab.Navigator>
);

const Stack = createNativeStackNavigator();

export const Router = () => {
  const [isInit, setIsInit] = useState(false);
  const { dispatch, useSelector, actions } = useStore();

  const user = useSelector((state) => state.user.data);
  useEffect(() => {
    dispatch(actions.user.init()).then((e) => {
      if (e.type === 'user/init/fulfilled') {
        dispatch(actions.leagues.init()).then(() => { setIsInit(true); });
      } else { setIsInit(true); }
    });
  }, []);

  if (!isInit) return <Loader w="full" h="full" />;

  return (
    <NavigationContainer linking={linking} theme={useColorModeValue(DefaultTheme, DarkTheme)}>
      <Stack.Navigator
        screenOptions={{ headerShown: false }}
      >
        {user !== null ? (
          <>
            <Stack.Screen name="Root" component={MainStackScreen} />
            <Stack.Screen name="Search" component={Search} />
            <Stack.Screen name="League" component={League} />
            <Stack.Screen name="Match" component={MatchView} />
          </>
        )
          : <Stack.Screen name="Login" component={Login} />}
      </Stack.Navigator>
    </NavigationContainer>
  );
};
