import React, {
  FC, ReactNode,
} from 'react';
import {
  Provider, TypedUseSelectorHook, useDispatch as useReduxDispatch, useSelector as useReduxSelector,
} from 'react-redux';
import {
  store, AppDispatch, RootState, combinedActions,
} from '@logic';

interface StoreProviderProps{
  children: ReactNode;
}

const storeContext = {
  ...store,
  actions: { ...combinedActions },
};

export const StoreProvider: FC<StoreProviderProps> = ({ children }: StoreProviderProps) => (
  <Provider store={store}>
    {children}
  </Provider>
);

// Use throughout the app instead of plain `useDispatch` and `useSelector`
const useDispatch = (): AppDispatch => useReduxDispatch<AppDispatch>();
const useSelector: TypedUseSelectorHook<RootState> = useReduxSelector;

interface IStore {
  store: typeof storeContext;
  actions: typeof combinedActions;
  dispatch: AppDispatch;
  useSelector: TypedUseSelectorHook<RootState>;
}
export const useStore = (): IStore => ({
  store: storeContext,
  actions: storeContext.actions,
  dispatch: useDispatch(),
  useSelector,
});
