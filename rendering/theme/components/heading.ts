export const Heading = {
  baseStyle: {
    fontWeight: 'bold',
    fontFamily: 'heading',
    lineHeight: 'xl',
    fontSize: 'xl',
  },
  defaultProps: {
    size: 'xl',
  },
  sizes: {
    xs: {
      fontSize: ['xs', null, null, 'xs'], lineHeight: ['xs', null, null, 'xs'], letterSpacing: ['xs', null, null, 'xs'], fontWeight: 500,
    },
    sm: {
      fontSize: ['sm', null, null, 'sm'], lineHeight: ['sm', null, null, 'sm'], letterSpacing: ['sm', null, null, 'sm'], fontWeight: 400,
    },
    md: {
      fontSize: ['md', null, null, 'md'], lineHeight: ['md', null, null, 'md'], letterSpacing: ['md', null, null, 'md'], fontWeight: 400,
    },
    lg: {
      fontSize: ['lg', null, null, 'lg'], lineHeight: ['lg', null, null, 'lg'], letterSpacing: ['lg', null, null, 'lg'], fontWeight: 500,
    },
    xl: {
      fontSize: ['xl', null, null, 'xl'], lineHeight: ['xl', null, null, 'xl'], letterSpacing: ['xl', null, null, 'xl'], fontWeight: 700,
    },
    '2xl': {
      fontSize: ['2xl', null, null, '2xl'], lineHeight: ['2xl', null, null, '2xl'], letterSpacing: ['2xl', null, null, '2xl'], fontWeight: 700,
    },
    '3xl': {
      fontSize: ['3xl', null, null, '3xl'], lineHeight: ['3xl', null, null, '3xl'], letterSpacing: ['3xl', null, null, '3xl'], fontWeight: 800,
    },
  },
};
