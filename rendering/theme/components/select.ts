export const Select = {
  baseStyle: {
    fontSize: 'md',
  },
  defaultProps: {
    variant: 'filled',
  },
  variants: {
    filled: {
      borderWidth: 'none',
    },
  },
};
