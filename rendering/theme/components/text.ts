export const Text = {
  baseStyle: {
    fontWeight: '400',
    fontFamily: 'body',
    fontStyle: 'normal',
    fontSize: 'sm',
    letterSpacing: 'md',
    lineHeight: 'lg',
  },
  defaultProps: {
    size: 'md',
  },
  sizes: {
    xs: {
      fontSize: ['xs', null, null, 'sm'], lineHeight: ['xs', null, null, 'sm'], letterSpacing: ['xs', null, null, 'sm'], fontWeight: 500,
    },
    sm: {
      fontSize: ['sm', null, null, 'md'], lineHeight: ['sm', null, null, 'md'], letterSpacing: ['sm', null, null, 'md'], fontWeight: 400,
    },
    md: {
      fontSize: ['md', null, null, 'lg'], lineHeight: ['md', null, null, 'lg'], letterSpacing: ['md', null, null, 'lg'], fontWeight: 400,
    },
    lg: {
      fontSize: ['lg', null, null, 'xl'], lineHeight: ['lg', null, null, 'xl'], letterSpacing: ['lg', null, null, 'xl'], fontWeight: 500,
    },
    xl: {
      fontSize: ['xl', null, null, '2xl'], lineHeight: ['xl', null, null, '2xl'], letterSpacing: ['xl', null, null, '2xl'], fontWeight: 700,
    },
    '2xl': {
      fontSize: ['2xl', null, null, '3xl'], lineHeight: ['2xl', null, null, '3xl'], letterSpacing: ['2xl', null, null, '3xl'], fontWeight: 700,
    },
    '3xl': {
      fontSize: '3xl', lineHeight: '3xl', letterSpacing: '3xl', fontWeight: 800,
    },
  },
};
