import { colors } from './colors';
import { Text, Heading, Select } from './components';
import { typography } from './typography';

// Define the config
const config = {
  useSystemColorMode: false,
  initialColorMode: 'light',
};

const breakpoints = {
  base: 0,
  sm: 480,
  md: 768,
  lg: 992,
  xl: 1280,
  '2xl': 1536,
};

export const theme = {
  ...typography,
  config,
  colors,
  breakpoints,
  components: {
    Text, Heading, Select,
  },
};
