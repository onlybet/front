/* eslint-disable camelcase */
import {
  useFonts,
  WorkSans_100Thin,
  WorkSans_200ExtraLight,
  WorkSans_300Light,
  WorkSans_400Regular,
  WorkSans_500Medium,
  WorkSans_600SemiBold,
  WorkSans_700Bold,
  WorkSans_800ExtraBold,
  WorkSans_900Black,
  WorkSans_100Thin_Italic,
  WorkSans_200ExtraLight_Italic,
  WorkSans_300Light_Italic,
  WorkSans_400Regular_Italic,
  WorkSans_500Medium_Italic,
  WorkSans_600SemiBold_Italic,
  WorkSans_700Bold_Italic,
  WorkSans_800ExtraBold_Italic,
  WorkSans_900Black_Italic,
} from '@expo-google-fonts/work-sans';

export const typography = {
  fontConfig: {
    WorkSans: {
      100: {
        normal: 'WorkSans_100Thin',
        italic: 'WorkSans_100Thin_Italic',
      },
      200: {
        normal: 'WorkSans_200ExtraLight',
        italic: 'WorkSans_200ExtraLight_Italic',
      },
      300: {
        normal: 'WorkSans_300Light',
        italic: 'WorkSans_300Light_Italic',
      },
      400: {
        normal: 'WorkSans_400Regular',
        italic: 'WorkSans_400Regular_Italic',
      },
      500: {
        normal: 'WorkSans_500Medium',
        italic: 'WorkSans_500Medium_Italic',
      },
      600: {
        normal: 'WorkSans_600SemiBold_',
        italic: 'WorkSans_600SemiBold_Italic',
      },
      700: {
        normal: 'WorkSans_700Bold',
        italic: 'WorkSans_700Bold_Italic',
      },
      800: {
        normal: 'WorkSans_800ExtraBold',
        italic: 'WorkSans_800ExtraBold_Italic',
      },
      900: {
        normal: 'WorkSans_900Black',
        italic: 'WorkSans_900Black_Italic',
      },
    },
  },
  fonts: {
    heading: 'WorkSans',
    body: 'WorkSans',
    mono: 'WorkSans',
  },
  letterSpacings: {
    xs: 0,
    sm: 0,
    md: 0,
    lg: 0,
    xl: -0.25,
    '2xl': -0.25,
    '3xl': -0.125,
  },
  lineHeights: {
    xs: '1em',
    sm: '1.125em',
    md: '1.125em',
    lg: '1.125em',
    xl: '1.125em',
    '2xl': '1.125em',
    '3xl': '1.125em',
  },
  fontSizes: {
    xs: 10,
    sm: 12,
    md: 15,
    lg: 17,
    xl: 20,
    '2xl': 24,
    '3xl': 28,
  },
};

export const loadFonts = () => useFonts({
  WorkSans_100Thin,
  WorkSans_200ExtraLight,
  WorkSans_300Light,
  WorkSans_400Regular,
  WorkSans_500Medium,
  WorkSans_600SemiBold,
  WorkSans_700Bold,
  WorkSans_800ExtraBold,
  WorkSans_900Black,
  WorkSans_100Thin_Italic,
  WorkSans_200ExtraLight_Italic,
  WorkSans_300Light_Italic,
  WorkSans_400Regular_Italic,
  WorkSans_500Medium_Italic,
  WorkSans_600SemiBold_Italic,
  WorkSans_700Bold_Italic,
  WorkSans_800ExtraBold_Italic,
  WorkSans_900Black_Italic,
});
