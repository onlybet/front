import React, { useEffect, useState, useMemo } from 'react';
import {
  VStack, Container, Flex, HStack, Box, useColorModeValue, Pressable,
} from 'native-base';
import { DefaultRouteProps } from '@navigation';
import { FlatList, View } from 'react-native';
import {
  Heading, SearchBar, BrickSwipe, Carousel, Card, ScrollView, Select, Text, BricksDropdown,
} from '@components';
import { SharedElement } from 'react-navigation-shared-element';
import { useStore } from '@contexts';
import { League } from '@models';

type Props = DefaultRouteProps<'Root'>;
type IContainerProps = React.ComponentProps<typeof Container>;

interface LeaguesProps extends IContainerProps, Props { }
const Leagues = ({ navigation, ...props }: LeaguesProps) => {
  const [showAll, setShowAll] = useState(false);
  const { dispatch, actions, useSelector } = useStore();
  const STORE_LEAGUES = useSelector((state) => state.leagues.data);
  const IS_LOADING = useSelector((state) => state.leagues.loading);
  const STORE_FAVORITES = useSelector((state) => state.leagues.favorites.data);

  useEffect(() => {
    if (STORE_LEAGUES.length <= 0) {
      dispatch(actions.leagues.getAllLeagues());
    }
  }, []);

  return (
    <Container w="full" {...props}>
      <Heading size="xl">Leagues</Heading>
      <VStack mt="4" space="4" w="full">
        {STORE_LEAGUES.map((e) => (
          <BrickSwipe
            {...e}
            title={e.name}
            subtitle={e.region}
            images={[{ src: e.imageURI, active: false }]}
            key={`${e.name} ${e.region}`}
            onPress={() => { navigation.navigate('League', { id: e.id }); }}
            onSwipeAction={() => {
              dispatch(actions.leagues.addFavorite(e.id));
            }}
            isFavorite={STORE_FAVORITES.findIndex((fav) => fav.id === e.id) > -1}
          />
        )).sort(() => 0.5 - Math.random()).slice(0, showAll ? 999 : 3)}
        {IS_LOADING && (
          <VStack mt="4" space="4" w="full">
            <BricksDropdown.Skeleton w="full" />
            <BricksDropdown.Skeleton w="full" />
            <BricksDropdown.Skeleton w="full" />
          </VStack>
        )}
        {!showAll && !IS_LOADING && (
          <Pressable onPress={() => setShowAll(true)}>
            <HStack space="4" w="full" justifyContent="center">
              <Box borderRadius="full" bg={useColorModeValue('light.800', 'dark.800')} w="2" h="2" />
              <Box borderRadius="full" bg={useColorModeValue('light.800', 'dark.800')} w="2" h="2" />
              <Box borderRadius="full" bg={useColorModeValue('light.800', 'dark.800')} w="2" h="2" />
            </HStack>
          </Pressable>
        )}
      </VStack>
    </Container>
  );
};

interface CarouselLeaguesProps extends IContainerProps, Props { }
const CarouselLeagues = ({ navigation, ...props }: CarouselLeaguesProps) => {
  const { useSelector, dispatch, actions } = useStore();
  const [date, setDate] = useState(new Date().toLocaleDateString('en-US'));
  const STORE_MATCHES = useSelector((state) => state.matches.data);
  const IS_LOADING = useSelector((state) => state.matches.loading);

  useEffect(() => {
    dispatch(actions.matches.getMatchesFromDate(String(new Date(date).getTime())));
  }, [date]);

  const LEAGUES = useMemo(() => {
    const leagues: Array<League> = [];

    STORE_MATCHES.forEach((match) => {
      const leagueExists = leagues.findIndex((e) => match.league.id === e.id);
      const index = leagueExists !== -1 ? leagueExists : leagues.length;
      if (leagueExists === -1) leagues.push({ ...match.league, matches: [] });

      leagues[index].matches.push({
        id: match.id,
        date: match.date,
        durationTime: match.durationTime,
        teams: match.teams,
      });
    });

    return leagues;
  }, [STORE_MATCHES]);

  return (
    <Container w="full" {...props}>
      <Carousel
        text={date}
        onPressLeft={() => {
          const newDate = new Date(date);
          newDate.setDate(newDate.getDate() - 1);
          const newDateString = newDate.toLocaleDateString('en-US');
          setDate(newDateString);
        }}
        onPressRight={() => {
          const newDate = new Date(date);
          newDate.setDate(newDate.getDate() + 1);
          const newDateString = newDate.toLocaleDateString('en-US');
          setDate(newDateString);
        }}
      />
      {!IS_LOADING && (
        <VStack mt="4" space="4" w="full">
          {/* TODO sort matches by date */}
          {LEAGUES.map((e) => (
            <BricksDropdown
              parent={{
                ...e,
                title: e.name,
                subtitle: e.region,
                images: [{ src: e.imageURI, active: false }],
                caption: e.matches.length.toString(),
              }}
              list={e.matches.map((match) => ({
                id: match.id,
                title: match.teams[0].name,
                subtitle: match.teams[1].name,
                caption: `${match.teams[0].points} - ${match.teams[1].points}`,
                images: [{ src: match.teams[0].imageURI, active: false }, { src: match.teams[1].imageURI, active: false }],
                onPress: () => {
                  dispatch(actions.matches.setMatch(match));
                  navigation.navigate('Match', { id: match.id });
                },
              }))}
              key={e.id}
            />
          ))}
        </VStack>
      )}
      {LEAGUES.length === 0 && !IS_LOADING && <Text size="md" textAlign="center" w="full" mt="4">no matches found</Text>}
      {IS_LOADING && (
        <VStack mt="4" space="4" w="full">
          <BricksDropdown.Skeleton w="full" />
          <BricksDropdown.Skeleton w="full" />
          <BricksDropdown.Skeleton w="full" />
          <BricksDropdown.Skeleton w="full" />
        </VStack>
      )}
    </Container>
  );
};

const Favorites = ({ ...props }: IContainerProps) => {
  const { useSelector, dispatch, actions } = useStore();
  const STORE_FAVORITES = useSelector((state) => state.leagues.favorites.data);

  useEffect(() => {
    dispatch(actions.leagues.getFavoriteLeagues());
  }, []);

  if (STORE_FAVORITES.length === 0) return <></>;
  return (
    <>
      <Container w="full" {...props}>
        <Flex direction="row" justifyContent="space-between" alignItems="center" mb="4" w="full">
          <Heading size="xl">Favorites</Heading>
          <Select placeholder="Select" selectedValue="laliga" minWidth="150px" variant="outline">
            <Select.Item label="Laliga" value="laliga" />
          </Select>
        </Flex>
      </Container>
      <FlatList
        horizontal
        data={STORE_FAVORITES}
        ItemSeparatorComponent={() => <View style={{ height: '100%', width: 16 }} />}
        renderItem={() => <Card.Skeleton mb="4" />}
        keyExtractor={(item) => item.id.toString()}
        style={{ width: '100%' }}
        contentContainerStyle={{ paddingLeft: '10%', paddingRight: '10%' }}
      />
    </>
  );
};

interface ISearchProps extends IContainerProps {
  navigation: Props['navigation'];
}
const Search = ({ navigation, ...props }: ISearchProps) => (
  <Container w="full" {...props}>
    <SharedElement id="SearchBarElement" style={{ width: '100%' }}>
      <SearchBar
        placeholder="Search for leagues, players, etc..."
        onPress={() => navigation.navigate('Search')}
        isReadOnly
        w="full"
      />
    </SharedElement>
  </Container>
);

export const Home = ({ navigation, ...props }: Props) => (
  <ScrollView
    pb="8"
    contentContainerStyle={{ alignItems: 'center' }}
    w="full"
  >
    <Search mt="10" navigation={navigation} />
    <Favorites mt="8" />
    <Leagues mt="4" navigation={navigation} {...props} />
    <CarouselLeagues mt="8" mb="8" navigation={navigation} {...props} />
  </ScrollView>
);

Home.sharedElements = () => [
  {
    id: 'SearchBarElement',
    animation: 'move',
    resize: 'clip',
  },
];
