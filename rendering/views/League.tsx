import React, { useEffect, useState } from 'react';
import { DefaultRouteProps } from '@navigation';
import {
  Icon, Heading, Brick, ScrollView, IBrickProps, Text, Carousel,
} from '@components';
import {
  Pressable, Container, VStack, HStack, Box, Flex, Image, useColorModeValue,
} from 'native-base';
import { useStore } from '@contexts';
import { useApi } from '@api';
import { Match } from '@models';

type IStackProps = React.ComponentProps<typeof VStack>;
type IContainerProps = React.ComponentProps<typeof Container>;
type Props = DefaultRouteProps<'League'>;

interface IBackButtonProps extends IContainerProps {
  navigation: Props['navigation'];
}
const BackButton = ({ navigation, ...props }: IBackButtonProps) => (
  <Container w="full" {...props}>
    <Pressable
      p={1}
      onPress={() => (navigation.canGoBack() ? navigation.goBack() : navigation.replace('Root'))}
    >
      <Icon.ArrowLeft size={6} />
    </Pressable>
  </Container>
);

interface TitleBlockProps extends IStackProps {
  title: string;
  image: string;
  region: string;
}
const TitleBlock = ({
  title, image, region, ...props
}: TitleBlockProps) => (
  <VStack w="full" {...props}>
    <Flex direction="column" justifyContent="center" alignItems="center" w="full">
      <Image
        size="24"
        source={{
          uri: image,
        }}
        alt={title}
      />
      <Heading
        size="3xl"
        mt="3"
        textTransform="uppercase"
        color={useColorModeValue('light.100', 'dark.100')}
      >
        {title}
      </Heading>
    </Flex>
    <Text w="full" textAlign="center" color={useColorModeValue('light.600', 'dark.600')}>{region}</Text>
  </VStack>
);

interface IListProps extends IContainerProps {
  title: string;
  isLoading: boolean;
  data: Array<IBrickProps>;
}

const List = ({
  title, isLoading, data, ...props
}: IListProps) => {
  const [showAll, setShowAll] = useState(false);
  return (
    <VStack {...props}>
      <Heading size="xl">{title}</Heading>
      {(data.length <= 0 && !isLoading) && (
      <Text size="md" mt="4">no data found</Text>
      )}
      {(data.length > 0 && !isLoading) && (
      <VStack mt="4" space="4" w="full">
        {(showAll && data.length > 4 ? data : data.slice(0, 4)).map((e) => <Brick {...e} key={`${e.title} ${e.subtitle}`} />)}
      </VStack>
      )}
      {!showAll && !isLoading && (
      <Pressable mt="4" onPress={() => setShowAll(true)}>
        <HStack space="4" w="full" justifyContent="center">
          <Box borderRadius="full" bg={useColorModeValue('light.800', 'dark.800')} w="2" h="2" />
          <Box borderRadius="full" bg={useColorModeValue('light.800', 'dark.800')} w="2" h="2" />
          <Box borderRadius="full" bg={useColorModeValue('light.800', 'dark.800')} w="2" h="2" />
        </HStack>
      </Pressable>
      )}
      {isLoading && (
      <VStack mt="4" space="4" w="full">
        <Brick.Skeleton w="full" />
        <Brick.Skeleton w="full" />
        <Brick.Skeleton w="full" />
        <Brick.Skeleton w="full" />
      </VStack>
      )}
    </VStack>
  );
};

interface CarouselLeaguesProps extends IContainerProps, Props {
  idLeague: string;
}
const CarouselMatches = ({ navigation, idLeague, ...props }: CarouselLeaguesProps) => {
  const { api } = useApi();
  const [rounds, setRounds] = useState({
    previous: '',
    current: '',
    next: '',
  });
  const [loading, setLoading] = useState(true);
  const [matches, setMatches] = useState<Array<Match>>([]);

  useEffect(() => {
    setLoading(true);
    api.matches.getMatchesFromLeague(idLeague).then((result) => {
      setMatches(result.gamesByRound);
      setRounds(result.rounds);
      setLoading(false);
    });
  }, [idLeague]);

  return (
    <Container w="full" {...props}>
      <Carousel
        w="full"
        maxW="full"
        text={rounds.current}
        showIcon={false}
        onPressLeft={() => {
          setLoading(true);
          api.matches.getMatchesFromLeague(idLeague, rounds.previous).then((result) => {
            setMatches(result.gamesByRound);
            setRounds(result.rounds);
            setLoading(false);
          });
        }}
        onPressRight={() => {
          setLoading(true);
          api.matches.getMatchesFromLeague(idLeague, rounds.next).then((result) => {
            setMatches(result.gamesByRound);
            setRounds(result.rounds);
            setLoading(false);
          });
        }}
      />
      {!loading && (
        <VStack mt="4" space="4" w="full">
          {matches.map((match) => (
            <Brick
              title={match.teams[0].name}
              subtitle={match.teams[1].name}
              caption={`${match.teams[0].points} - ${match.teams[1].points}`}
              images={[{ src: match.teams[0].imageURI, active: false }, { src: match.teams[1].imageURI, active: false }]}
              onPress={() => {
                navigation.navigate('Match', { id: match.id });
              }}
              key={match.id}
            />
          ))}
        </VStack>
      )}
      {matches.length === 0 && !loading && <Text size="md" textAlign="center" w="full" mt="4">no matches found</Text>}
      {loading && (
        <VStack mt="4" pb="8" space="4" w="full">
          <Brick.Skeleton w="full" />
          <Brick.Skeleton w="full" />
          <Brick.Skeleton w="full" />
          <Brick.Skeleton w="full" />
        </VStack>
      )}
    </Container>
  );
};

export const League = ({ navigation, route }: Props) => {
  const { id } = route.params;
  const { api } = useApi();
  const { useSelector, actions, dispatch } = useStore();
  const STORE_LEAGUE = useSelector((state) => state.leagues.selected);
  const [matches, setMatches] = useState<Array<Match>>([]);
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    dispatch(actions.leagues.getLeague(id));
    setMatches([]);
    setLoading(true);

    api.matches.getMatchesFromLeague(id).then((result) => {
      setMatches(result.games);
      setLoading(false);
    });
  }, [id]);

  if (!STORE_LEAGUE.data) return <BackButton navigation={navigation} pt="4" mb="3" />;

  return (
    <ScrollView
      contentContainerStyle={{ alignItems: 'center' }}
      w="full"
    >
      <BackButton navigation={navigation} pt="4" />
      <Container w="full" pb="8">
        <TitleBlock
          title={STORE_LEAGUE.data.name}
          image={STORE_LEAGUE.data.imageURI}
          region={STORE_LEAGUE.data.region}
        />
        <List
          mt="10"
          title="Matches"
          mb="8"
          w="full"
          isLoading={loading}
          data={matches.map((match) => ({
            title: match.teams[0].name,
            subtitle: match.teams[1].name,
            images: [...match.teams.map((t) => ({ src: t.imageURI, active: false }))],
            caption: `${match.teams[0].points} - ${match.teams[1].points}`,
            onPress: () => navigation.navigate('Match', { id: match.id }),
          }))}
        />
      </Container>
      <CarouselMatches navigation={navigation} route={route} idLeague={id} pb="24" />
    </ScrollView>
  );
};
