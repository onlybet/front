import React, { useState, useEffect } from 'react';
import {
  VStack, Container, useColorModeValue, useBreakpointValue, Center, Button,
} from 'native-base';
import {
  Heading, Input, Text,
} from '@components';
import { useStore } from '@contexts';
import { DefaultRouteProps } from '@navigation';

interface SignUpElementProps {
  setLogInElement: () => void;
}
const SignUpElement = ({ setLogInElement }: SignUpElementProps) => {
  const { dispatch, actions } = useStore();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  return (
    <Container>
      <VStack space="3" alignItems="center">
        <Input
          type="email"
          placeholder="email"
          _hover={{ bg: 'transparent' }}
          width="100%"
          height="100%"
          bg={useColorModeValue('light.900', 'dark.900')}
          borderRadius="8"
          py={useBreakpointValue({ base: 2, lg: 4 })}
          placeholderTextColor={useColorModeValue('light.600', 'dark.600')}
          color={useColorModeValue('light.100', 'dark.100')}
          size="md"
          onChange={(e) => {
            e.persist();
            setEmail(((e.target) as any).value);
          }}
        />
        <Input
          type="password"
          placeholder="password"
          _hover={{ bg: 'transparent' }}
          width="100%"
          height="100%"
          bg={useColorModeValue('light.900', 'dark.900')}
          borderRadius="8"
          py={useBreakpointValue({ base: 2, lg: 4 })}
          placeholderTextColor={useColorModeValue('light.600', 'dark.600')}
          color={useColorModeValue('light.100', 'dark.100')}
          size="md"
          onChange={(e) => {
            e.persist();
            setPassword(((e.target) as any).value);
          }}
        />
        <Button
          size="lg"
          w="full"
          py="3"
          onPress={() => {
            if (password.length > 6 && email.includes('@')) { dispatch(actions.user.signUp({ email, password })); }
          }}
        >
          Sign up
        </Button>
        <Text
          size="xs"
          color={useColorModeValue('light.100', 'dark.100')}
          underline
          onPress={setLogInElement}
        >
          login in
        </Text>
      </VStack>
    </Container>
  );
};

interface LoginElementProps {
  setSignUpElement: () => void;
}
const LoginElement = ({ setSignUpElement }: LoginElementProps) => {
  const { dispatch, actions, useSelector } = useStore();
  const error = useSelector((state) => state.user.error);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  return (
    <Container>
      <VStack space="3" alignItems="center">
        <Input
          type="email"
          placeholder="email"
          _hover={{ bg: 'transparent' }}
          width="100%"
          height="100%"
          bg={useColorModeValue('light.900', 'dark.900')}
          borderRadius="8"
          py={useBreakpointValue({ base: 2, lg: 4 })}
          placeholderTextColor={useColorModeValue('light.600', 'dark.600')}
          color={useColorModeValue('light.100', 'dark.100')}
          size="md"
          onChange={(e) => {
            e.persist();
            setEmail(((e.target) as any).value);
          }}
        />
        <Input
          type="password"
          placeholder="password"
          _hover={{ bg: 'transparent' }}
          width="100%"
          height="100%"
          bg={useColorModeValue('light.900', 'dark.900')}
          borderRadius="8"
          py={useBreakpointValue({ base: 2, lg: 4 })}
          placeholderTextColor={useColorModeValue('light.600', 'dark.600')}
          color={useColorModeValue('light.100', 'dark.100')}
          size="md"
          onChange={(e) => {
            e.persist();
            setPassword(((e.target) as any).value);
          }}
        />
        <Button
          size="lg"
          w="full"
          py="3"
          onPress={() => {
            if (password.length > 6 && email.includes('@')) { dispatch(actions.user.login({ email, password })); }
          }}
        >
          Log in
        </Button>
        <Text
          size="xs"
          color={useColorModeValue('light.100', 'dark.100')}
          underline
          onPress={setSignUpElement}
        >
          sign up
        </Text>
        <Text
          size="md"
          color="red"
          onPress={setSignUpElement}
          mt="8"
        >
          {error}
        </Text>
      </VStack>
    </Container>
  );
};

export const Login = () => {
  const [element, setElement] = useState('login');

  return (
    <Center
      pb="8"
      w="full"
      h="full"
    >
      <Heading size="3xl" mb="4" color="primary.500">OnlyBet</Heading>
      {element === 'login' && <LoginElement setSignUpElement={() => setElement('signup')} />}
      {element === 'signup' && <SignUpElement setLogInElement={() => setElement('login')} />}
    </Center>
  );
};
