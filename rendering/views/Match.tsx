import React, { useEffect, useState } from 'react';
import {
  Container, VStack, Flex, Heading, Select, Image, useColorModeValue, Pressable,
} from 'native-base';
import {
  Card, Overview, DualProgress, Text, Icon, ScrollView,
} from '@components';
import {
  FlatList, View,
} from 'react-native';
import { DefaultRouteProps } from '@navigation';
import { useStore } from '@contexts';
import { useApi } from '@api';
import { Match } from '@models';

type IContainerProps = React.ComponentProps<typeof Container>;
type Props = DefaultRouteProps<'Match'>;
interface IBackButtonProps extends IContainerProps {
  navigation: Props['navigation'];
}
const BackButton = ({ navigation, ...props }: IBackButtonProps) => (
  <Container w="full" {...props}>
    <Pressable
      p={1}
      onPress={() => (navigation.canGoBack() ? navigation.goBack() : navigation.replace('Root'))}
    >
      <Icon.ArrowLeft size={6} />
    </Pressable>
  </Container>
);

interface LatestMatchProps { id: string; title: string; navigation: Props['navigation']; }
const LatestMatch = ({ id, navigation, title }: LatestMatchProps) => {
  const { api } = useApi();
  const [OTHER_MATCHES, SET_OTHER_MATCHES] = useState<{ loading: boolean, data: Array<Match> }>({ loading: true, data: [] });

  useEffect(() => {
    const setup = async () => {
      api.matches.getMatchesFromTeam(id).then((data) => { SET_OTHER_MATCHES({ loading: false, data }); });
    };
    setup();
  }, [id]);

  return (
    <>
      <Container w="full">
        <Heading size="xl" mb="4">
          Latest Matches of
          {' '}
          {title}
        </Heading>
      </Container>
      {OTHER_MATCHES.loading && (
        <FlatList
          horizontal
          data={[{ id: '1' }, { id: '2' }, { id: '3' }, { id: '4' }]}
          ItemSeparatorComponent={() => <View style={{ height: '100%', width: 16 }} />}
          renderItem={() => <Card.Skeleton mb="4" />}
          keyExtractor={(item) => item.id.toString()}
          style={{ width: '100%' }}
          contentContainerStyle={{ paddingLeft: '10%', paddingRight: '10%' }}
        />
      )}
      {!OTHER_MATCHES.loading && OTHER_MATCHES.data && (
        <FlatList
          horizontal
          data={OTHER_MATCHES.data}
          ItemSeparatorComponent={() => <View style={{ height: '100%', width: 16 }} />}
          renderItem={(e) => (
            <Card.Football
              teams={e.item.teams.map((team) => ({ ...team, imageSrc: team.imageURI, score: team.points }))}
              date={new Date(e.item.date)}
              information="information"
              status="hidden"
              mb="4"
              onPress={() => { navigation.navigate('Match', { id: e.item.id }); }}
              bg={`primary.${(Number(e.item.id) % 3) + 3}00`}
            />
          )}
          keyExtractor={(item) => item.id.toString()}
          style={{ width: '100%' }}
          contentContainerStyle={{ paddingLeft: '10%', paddingRight: '10%' }}
        />
      )}
    </>
  );
};

interface StatisticsProps extends IContainerProps {
  elements: Array<{
    value1: number;
    value2: number;
    title: string;
  }>
}
const Statistics = ({ elements, ...props }: StatisticsProps) => (
  <Container w="full" {...props}>
    <Flex direction="row" justifyContent="flex-start" alignItems="center" mb="4" w="full">
      <Heading size="xl">Details</Heading>
      {/* <Select placeholder="Select" selectedValue="laliga" minWidth="150px" variant="outline">
        <Select.Item label="Laliga" value="laliga" />
      </Select> */}
    </Flex>
    <VStack space={6} w="full">
      {elements.map((element) => <DualProgress value1={element.value1} value2={element.value2} title={element.title} key={element.title} />)}
    </VStack>
    {elements.length === 0 && (<Text w="full" textAlign="center" color={useColorModeValue('light.600', 'dark.600')}>No statistics available yet.</Text>)}
  </Container>
);

type IVStackProps = React.ComponentProps<typeof VStack>;
interface TitleBlockProps extends IVStackProps {
  teams: Array<{
    name: string;
    imageSrc: string;
  }>;
  date: Date;
}
const TitleBlock = ({ teams, date, ...props }: TitleBlockProps) => (
  <VStack w="full" {...props}>
    <Flex direction="row" justifyContent="space-between" alignItems="center" w="full">
      <Heading
        size="3xl"
        textTransform="uppercase"
        color={useColorModeValue('light.100', 'dark.100')}
      >
        {teams[0].name}
      </Heading>
      <Image
        size="6"
        source={{
          uri: teams[0].imageSrc,
        }}
        alt={teams[0].name}
      />
    </Flex>
    <Flex direction="row" justifyContent="space-between" alignItems="center" w="full" mt="-2">
      <Heading
        size="3xl"
        textTransform="uppercase"
        color={useColorModeValue('light.100', 'dark.100')}
      >
        {teams[1].name}
      </Heading>
      <Image
        size="6"
        source={{
          uri: teams[1].imageSrc,
        }}
        alt={teams[1].name}
      />
    </Flex>
    <Text w="full" textAlign="right" color={useColorModeValue('light.600', 'dark.600')}>{date.toUTCString()}</Text>
  </VStack>
);

export const MatchView = ({ navigation, route, ...props }: Props) => {
  const { id } = route.params;
  const { useSelector, actions, dispatch } = useStore();

  useEffect(() => {
    dispatch(actions.matches.getMatch(id));
  }, [id]);

  const STORE_MATCH = useSelector((state) => state.matches.selected);

  if (!STORE_MATCH.data) return <BackButton navigation={navigation} pt="4" mb="3" />;

  return (
    <ScrollView
      pb="8"
      contentContainerStyle={{ alignItems: 'center' }}
      w="full"
      {...props}
    >
      <Container w="full" mb="10">
        <BackButton navigation={navigation} pt="4" mb="3" />
        <TitleBlock
          mb="8"
          teams={STORE_MATCH.data.teams.map((team) => ({ ...team, imageSrc: team.imageURI, score: team.points }))}
          date={new Date(STORE_MATCH.data.date)}
        />
        <Overview
          w="full"
          duration={STORE_MATCH.data.durationTime.toString()}
          teams={STORE_MATCH.data.teams.map((team) => ({ ...team, imageSrc: team.imageURI, score: team.points }))}
        />
      </Container>
      <Statistics
        mb="8"
        elements={STORE_MATCH.data.teams[0].statistics.map((statistic) => ({
          title: statistic.name,
          value1: statistic.value || 0,
          value2: STORE_MATCH.data!.teams[1].statistics.find((stat) => stat.name === statistic.name)?.value || 0,
        }))}
      />
      <LatestMatch
        id={STORE_MATCH.data.teams[0].id}
        navigation={navigation}
        title={STORE_MATCH.data.teams[0].name}
      />
      <LatestMatch
        id={STORE_MATCH.data.teams[1].id}
        navigation={navigation}
        title={STORE_MATCH.data.teams[1].name}
      />
    </ScrollView>
  );
};
