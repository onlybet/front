import React from 'react';
import { Heading, Text, ScrollView } from '@components';
import {
  HStack, Switch, useColorMode, Container, Button,
} from 'native-base';
import { useStore } from '@contexts';

// Color Switch Component
function ToggleDarkMode() {
  const { colorMode, toggleColorMode } = useColorMode();
  return (
    <HStack space={2} alignItems="center">
      <Text>Dark</Text>
      <Switch
        isChecked={colorMode === 'light'}
        onToggle={toggleColorMode}
        aria-label={
          colorMode === 'light' ? 'switch to dark mode' : 'switch to light mode'
        }
      />
      <Text>Light</Text>
    </HStack>
  );
}

export const Profile = () => {
  const { dispatch, actions } = useStore();
  return (
    <ScrollView
      contentContainerStyle={{ alignItems: 'center' }}
    >
      <Container w="full">
        <Heading size="3xl" mt="8">Profile</Heading>
        <Heading size="xl" mt="6" mb="4">Toggle mode</Heading>
        <ToggleDarkMode />
        <Heading size="xl" mt="6" mb="4">Settings</Heading>
        <Button
          size="lg"
          py="3"
          onPress={() => {
            dispatch(actions.user.signOut());
          }}
        >
          Disconnect
        </Button>
      </Container>
    </ScrollView>
  );
};
