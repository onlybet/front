import React, { useRef, useEffect } from 'react';
import { DefaultRouteProps } from '@navigation';
import {
  Icon, Heading, Brick, SearchBar, ScrollView, Text, BrickSwipe,
} from '@components';
import {
  Pressable, Container, VStack,
} from 'native-base';
import { SharedElement } from 'react-navigation-shared-element';
import { useStore } from '@contexts';

type IContainerProps = React.ComponentProps<typeof Container>;
type Props = DefaultRouteProps<'Search'>;

interface IBackButtonProps extends IContainerProps {
  navigation: Props['navigation'];
}
const BackButton = ({ navigation, ...props }: IBackButtonProps) => (
  <Container w="full" {...props}>
    <Pressable
      p={1}
      onPress={() => (navigation.canGoBack() ? navigation.goBack() : navigation.replace('Root'))}
    >
      <Icon.ArrowLeft size={6} />
    </Pressable>
  </Container>
);

type IBrickSwipe = React.ComponentProps<typeof BrickSwipe>;

interface IListProps extends IContainerProps {
  title: string;
  isLoading: boolean;
  data: Array<IBrickSwipe>;
}
const List = ({
  title, isLoading, data, ...props
}: IListProps) => (
  <Container w="full" {...props}>
    <Heading size="xl">{title}</Heading>
    {(data.length <= 0 && !isLoading) && (
      <Text size="md" mt="4">no data found</Text>
    )}
    {(data.length > 0 && !isLoading) && (
      <VStack mt="4" space="4" w="full">
        {data.map((e) => (
          <BrickSwipe
            {...e}
            key={`${e.title} ${e.subtitle}`}
          />
        ))}
      </VStack>
    )}
    {isLoading && (
      <VStack mt="4" space="4" w="full">
        <Brick.Skeleton w="full" />
        <Brick.Skeleton w="full" />
        <Brick.Skeleton w="full" />
        <Brick.Skeleton w="full" />
      </VStack>
    )}
  </Container>
);

export const Search = ({ navigation }: Props) => {
  const ref = useRef<any | undefined>(undefined);
  const { dispatch, actions, useSelector } = useStore();
  const IS_LOADING_SEARCHED = useSelector((state) => state.leagues.searched.loading);
  const SEARCHED_DATA = useSelector((state) => state.leagues.searched.data);
  const FAVORITES_DATA = useSelector((state) => state.leagues.favorites.data);
  const IS_LOADING_FAVORITES = useSelector((state) => state.leagues.favorites.loading);

  useEffect(() => {
    if (ref.current) { ref.current.focus(); }
  }, [ref]);

  useEffect(() => {
    if (FAVORITES_DATA.length === 0) {
      dispatch(actions.leagues.getFavoriteLeagues());
    }
    dispatch(actions.leagues.searchLeagues(''));
  }, []);

  return (
    <ScrollView
      contentContainerStyle={{ alignItems: 'center' }}
      w="full"
    >
      <BackButton navigation={navigation} pt="12" />
      <Container w="full" mt="3">
        <SharedElement id="SearchBarElement" style={{ width: '100%' }}>
          <SearchBar
            ref={ref}
            placeholder="Search for leagues or matches"
            w="full"
            isReadOnly={false}
            onChange={(value) => {
              dispatch(actions.leagues.searchLeagues(value));
            }}
          />
        </SharedElement>
      </Container>
      <List
        mt="8"
        title="Favorites"
        isLoading={IS_LOADING_FAVORITES}
        data={FAVORITES_DATA.map((data) => ({
          title: data.name,
          subtitle: data.region,
          images: [{ src: data.imageURI, active: true }],
          caption: data.caption,
          onSwipeAction: () => {
            dispatch(actions.leagues.removeFavorite(data.id));
          },
          isFavorite: FAVORITES_DATA.findIndex((fav) => fav.id === data.id) > -1,
          onPress: () => navigation.navigate('League', { id: data.id }),
        }))}
      />
      <List
        mt="8"
        title="Leagues"
        mb="8"
        isLoading={IS_LOADING_SEARCHED}
        data={SEARCHED_DATA.map((data) => ({
          title: data.name,
          subtitle: data.region,
          images: [{ src: data.imageURI, active: true }],
          caption: data.caption,
          onSwipeAction: () => {
            if (FAVORITES_DATA.findIndex((fav) => fav.id === data.id) > -1) {
              dispatch(actions.leagues.removeFavorite(data.id));
            } else {
              dispatch(actions.leagues.addFavorite(data.id));
            }
          },
          isFavorite: FAVORITES_DATA.findIndex((fav) => fav.id === data.id) > -1,
          onPress: () => navigation.navigate('League', { id: data.id }),
        }))}
      />
    </ScrollView>
  );
};

Search.sharedElements = () => [
  {
    id: 'SearchBarElement',
    animation: 'move',
    resize: 'clip',
  },
];
