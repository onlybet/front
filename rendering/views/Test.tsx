import React from 'react';
import {
  Text,
  Link,
  HStack,
  Center,
  Heading,
  Switch,
  useColorMode,
  VStack,
  Code,
} from 'native-base';
import {
  Carousel,
  BricksDropdown, DualProgress, Brick, SearchBar,
} from '@components';

// Color Switch Component
function ToggleDarkMode() {
  const { colorMode, toggleColorMode } = useColorMode();
  return (
    <HStack space={2} alignItems="center">
      <Text>Dark</Text>
      <Switch
        isChecked={colorMode === 'light'}
        onToggle={toggleColorMode}
        aria-label={
          colorMode === 'light' ? 'switch to dark mode' : 'switch to light mode'
        }
      />
      <Text>Light</Text>
    </HStack>
  );
}

export const Test = () => {
  const parent = {
    images: [{
      src: 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Flag_of_France.svg/langfr-225px-Flag_of_France.svg.png',
      active: true,
    }],
    title: 'Ligue 1',
    subtitle: 'France',
    caption: '23',
  };
  const list = [{ ...parent, id: '1' }, { ...parent, id: '2' }, { ...parent, id: '3' }];
  return (
    <Center
      _dark={{
        bg: {
          linearGradient: {
            colors: ['#000000', '#131313', '#131313'],
            start: [0, 0],
            end: [1, 1],
          },
        },
      }}
      _light={{
        bg: {
          linearGradient: {
            colors: ['#FFFFFF', '#F9F9F9', '#F9F9F9'],
            start: [0, 0],
            end: [1, 1],
          },
        },
      }}
      px={4}
      flex={1}
    >
      <VStack space={5} alignItems="center" w="303px">
        <Carousel text="Today" />
        <Brick.Skeleton w="100%" />
        <SearchBar placeholder="search leagues, players..." />
        <DualProgress
          title="Possession %"
          value1={3}
          value2={1}
        />
        <BricksDropdown parent={parent} list={list} />
        <Heading size="xl">Welcome to NativeBase</Heading>
        <HStack space={2} alignItems="center">
          <Text>Edit</Text>
          <Code>App.tsx</Code>
          <Text>and save to reload.</Text>
        </HStack>
        <Link href="https://docs.nativebase.io" isExternal>
          <Text color="primary.500" underline fontSize="xl">
            Learn NativeBase
          </Text>
        </Link>
        <ToggleDarkMode />
      </VStack>
    </Center>
  );
};
